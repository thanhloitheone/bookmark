<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BookMarkTest extends TestCase
{
    use DatabaseMigrations;

    protected $bookmark;

    public function setUp()
    {
        parent::setUp();
        $this->bookmark = factory('App\BookMark')->create();
    }

    public function testUrlStatusIndex()
    {
        $this->get('/bookmark')->assertStatus(200);
    }

    public function testUrlStatusCreate()
    {
        $this->get('/bookmark/create')->assertStatus(200);
    }

    public function testShowAllBookMark()
    {
        $this->get('/bookmark')
            ->assertSee($this->bookmark->title)
            ->assertSee($this->bookmark->url_online);
    }
}
