<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateBookMarkTest extends TestCase
{
    use DatabaseMigrations;

    public function test_create_bookmark()
    {
        $bookmark = factory('App\BookMark')->create();

        $this->post('/bookmark', $bookmark->toArray());

        $this->get('/bookmark')
            ->assertSee($bookmark->title)
            ->assertSee($bookmark->url_online);
    }

    public function test_create_bookmark_url_bookmark()
    {
        $bookmark = factory('App\BookMark')->create();

        $this->post('/bookmark', $bookmark->toArray());

        $this->get($bookmark->path())
            ->assertSee($bookmark->title)
            ->assertSee($bookmark->url_online);
    }
}
