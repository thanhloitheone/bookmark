<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BookMarkTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_Title_Bookmark()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/laravel/bookmark/public')
                    ->assertTitle('BOOKMARK');
        });
    }

    public function test_Bookmark_Click_Create_Url()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/laravel/bookmark/public')
                    ->clickLink('Create')
                    ->assertPathIs('/laravel/bookmark/public/bookmark/create');
        });
    }

    public function test_Enter_Database_True_In_Website()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/laravel/bookmark/public')
                    ->clickLink('Create')
                    ->type('title', 'Hello')
                    ->type('urlonline', 'http://google.com')
                    ->press('Save')
                    ->assertPathIs('/laravel/bookmark/public/bookmark');
        });
    }

    public function test_Enter_Database_False_In_Website()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/laravel/bookmark/public')
                    ->clickLink('Create')
                    ->type('title', 'Hello')
                    ->type('urlonline', 'HAHIHEHO')
                    ->press('Save')
                    ->assertSee('The urlonline format is invalid.');
        });
    }
}
