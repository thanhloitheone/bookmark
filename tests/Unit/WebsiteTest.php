<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Support\Website;
use App\BookMark;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class WebsiteTest extends TestCase
{
    use DatabaseMigrations;

    private $factory;
    private $website;
    private $stubwebsite;

    protected $databasetest = [
        'title' => 'Hello',
        'urlonline' => 'http://dantri.com.vn/phap-luat/luat-su-cho-rang-ong-dinh-la-thang-khong-pham-toi-co-y-lam-trai-20180112074800495.htm'
    ];

    public function setUp()
    {
        parent::setUp();

        $this->factory = factory('App\BookMark', 10)->create();

        $this->website = new Website($this->databasetest);

        $this->stubwebsite = $this->createMock(Website::class);
    }

    public function test_Create_Defaul_Database()
    {
        $this->assertCount(10, $this->factory);
    }

    public function test_Exception_Request_Url()
    {
        $datatest = [
            'title' => 'The One',
            'urlonline' => 'Hahaha'
        ];

        $website = new Website($this->databasetest);

        $isrequesturl = $website->setRequest($datatest);

        $this->assertEquals(false, $isrequesturl);
    }

    public function test_Databse_Equals()
    {
        $this->assertEquals($this->website->all(), BookMark::all());
    }

    public function test_Create_Name_File()
    {
        $namefile = $this->databasetest['title'] . $this->databasetest['urlonline'];

        $namefile = str_slug($namefile) . '.html';

        $this->assertEquals($this->website->createNameFile(), $namefile);
    }

    public function test_Get_Content_Website()
    {
        $this->assertEquals($this->website->getContentWebsite(), $body);
    }

    public function test_Url_Image_Content_Website()
    {
        $body = file_get_contents($this->databasetest['urlonline']);

        $this->assertEquals($this->website->getContentWebsite(), $body);
    }

    public function test_Create_Add_Database()
    {
        $add_one_database = 1;

        $count_database_before = count(BookMark::all());

        $this->website->createDatabase();

        $count_database_after = count(BookMark::all());

        $this->assertEquals($count_database_before + $add_one_database, $count_database_after);
    }

    public function test_Create_Add_Database_2()
    {
        $this->website->createDatabase();

        $this->assertDatabaseHas('book_marks', [
            'title' => $this->databasetest['title'],
            'url_online' => $this->databasetest['urlonline'],
        ]);
    }

    public function test_Check_Dir_Image_Exists()
    {
        $dir = 'bookmark' . '/' . 'image' . '/' . str_slug($this->databasetest['urlonline']);
        $isfileimage = Storage::disk('local')->allFiles($dir);

        $isdir = count($isfileimage) >= 1;

        $this->assertEquals(true, $isdir);
    }

    public function test_Check_File_Html_Exists()
    {
        $namefile = $this->databasetest['title'] . $this->databasetest['urlonline'];

        $namefile = 'bookmark' . '/' . str_slug($namefile) . '.html';

        $isfilehtml = Storage::disk('local')->exists($namefile);

        $this->assertEquals(true, $isfilehtml);
    }

    public function test_False_Create_Bookmark_With_Savafile_false()
    {
        $this->stubwebsite->method('savefile')
            ->willReturn(false);

        $this->assertEquals(false, $this->stubwebsite->create());
    }

    public function test_False_Create_Bookmark_With_CreateDatabase_false()
    {
        $this->stubwebsite->method('savefile')
            ->willReturn(true);

        $this->stubwebsite->method('createDatabase')
            ->willReturn(false);

        $this->assertEquals(false, $this->stubwebsite->create());
    }

    public function test_Fucntion_Create_All()
    {
        $this->assertEquals($this->website->create(), true);
    }
}
