<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class BookMark extends Model
{
    protected $fillable = ['url_online', 'title', 'name_file'];

    public function path()
    {
        return 'bookmark/' . $this->id;
    }

    public function getID()
    {
        return $this->id;
    }

    public function urlfile()
    {
        $urlfilename = url('/') . '/../storage/app/bookmark/'  . $this->name_file;
        return $urlfilename;
    }

    public function storagefile()
    {
        return 'bookmark/' . '/' . $this->name_file;
    }
}
