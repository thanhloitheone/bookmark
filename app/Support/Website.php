<?php

namespace App\Support;

use App\BookMark;
use Illuminate\Support\Facades\Storage;

class Website
{
    private $request;
    const VALUE_FALSE = false;
    const DEFAULT_REQUEST = [
        'title' => 'TITLE',
        'urlonline' => 'http://google.com'
    ];

    public function __construct($request = self::DEFAULT_REQUEST)
    {
        $this->setRequest($request);
    }

    public function setRequest($request)
    {
        if ( $this->checkRequest($request) ) {
            $this->request = $request;
            return true;
        }
        return false;
    }

    public function checkRequest($request)
    {
        return filter_var($request['urlonline'], FILTER_VALIDATE_URL);
    }

    public function all()
    {
        return BookMark::all();
    }

    public function create()
    {
        if ($this->saveFile() == self::VALUE_FALSE) {
            return false;
        }

        if ($this->createDatabase() == self::VALUE_FALSE) {
            return false;
        }

        return true;
    }

    public function replaceImgBodyWebsite($body)
    {
        $array_image = $this->downloadImage();

        $pattern = [];
        $replacement = [];

        foreach ($array_image as $key => $value) {

            $pattern[] =  "~". $value . "~" ;

            $urloffline = url('/') . '/../storage/app/' . $this->getUrlImage($key);

            $replacement[] = $urloffline;
        }
        
        return preg_replace($pattern, $replacement, $body);
    }
    
    public function createNameFile()
    {
        $namefile = $this->request['title'] . $this->request['urlonline'];

        $namefile = str_slug($namefile) . '.html';

        return $namefile;
    }

    public function saveFile()
    {
        $url = 'bookmark' . '/' . $this->createNameFile();

        $body = $this->getContentWebsite();

        $body = $this->disableLink($body);

        $body = $this->replaceImgBodyWebsite($body);

        return Storage::disk('local')->put($url, $body);
    }

    private function disableLink($body)
    {
        $pattern = '/<a(.*)href="([^"]*)"(.*)>/';
        $replacement = '<a$1href="#"$3>';

        preg_replace($pattern, $replacement, $body);

        $pattern = [
            '/<a\b/',
            '/<\/a>/',
            '/<button\b/',
            '/type=\"button\"/',
        ];

        $replacement = [
            '<b',
            '</b>',
            '<b',
            'disabled'
        ];

        return preg_replace($pattern, $replacement, $body);
    }

    public function createDatabase()
    {
        return BookMark::create([
            'url_online' => $this->request['urlonline'],
            'title' => $this->request['title'],
            'name_file' => $this->createNameFile(),
        ]);
    }

    public function getContentWebsite()
    {
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_HEADER, 0);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_URL, $this->request['urlonline']);
        // $data = curl_exec($ch);
        // curl_close($ch);
        // return $data;
        return file_get_contents($this->request['urlonline']);
    }

    public function downloadImage()
    {
        $content = $this->getContentWebsite();

        //Sreach tag <img src="" />
        preg_match_all('|<img.*?src=[\'"](.*?)[\'"].*?>|i', $content, $images);

        $array_url_image = 1;
        $name = 0;
        $array_image = [];

        foreach ($images[$array_url_image] as $image) {
            if (empty($image)) {
                continue;
            }
            $array_image[$name] = $image;

            $this->saveImage($image, $name++);
        }

        return $array_image;
    }

    private function saveImage($urlimage, $name)
    {
        $name_image_file = $this->getUrlImage($name);

        $image = file_get_contents($urlimage);

        Storage::disk('local')->put($name_image_file, $image);
    }

    private function getUrlImage($name)
    {
        $dir = 'bookmark' . '/' . 'image' . '/' . str_slug($this->request['urlonline']);

        $name_image_file = $dir . '/' . $name . '.gif';

        return $name_image_file;
    }
}
