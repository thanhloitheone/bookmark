<?php

namespace App\Http\Controllers;

use App\BookMark;
use App\Support\Website;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class BookMarkController extends Controller
{
    private $website;

    public function __construct(Website $website)
    {
        $this->website = $website;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookmarks = $this->website->all();

        return view('bookmark.index', compact('bookmarks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bookmark.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$validate = $request->validate($this->rules(), $this->messages());
        if (!$this->checkValidate($request)) {
            return;
        }
        if (!$this->website->setRequest($request)) {
            return;
        }
        if (!$this->website->create()) {
            return;
        }

        return redirect('bookmark');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookMark  $bookMark
     * @return \Illuminate\Http\Response
     */
    public function show(BookMark $bookMark)
    {
        return view('bookmark.show', compact('bookMark'));
    }

    public function rediectError()
    {
        $error = 'You do not have permission to perform this operation';
        return view('bookmark.error', compact('error'));
    }

    public function checkValidate($request)
    {
        $validate = $request->validate($this->rules(), $this->messages());

        $istitle = $validate['title'] != $request['title'];
        $isurl = $validate['urlonline'] != $request['urlonline'];

        if ($istitle | $isurl) {
            return false;
        }

        return true;
    }

    private function rules()
    {
        return [
            'title' => 'required | max:255',
            'urlonline' => 'required | url',
        ];
    }

    private function messages()
    {
        return [
            'title.required' => 'A title is required',
            'title.max' => 'A title is max 255',
            'urlonline.required' => 'A url is required',
        ];
    }
}
