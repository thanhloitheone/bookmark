@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading text-center"><h1>BookMark</h1></div>
        </div>
        @foreach( $bookmarks as $bookmark )
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body">
                        <h1> <a href="{{ $bookmark->path() }}">{{ $bookmark->title }}</a> </h1>
                        <h6> {{ $bookmark->url_online }} </h6>
                        <hr>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection