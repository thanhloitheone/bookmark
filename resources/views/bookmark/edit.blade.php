@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                
                <div class="panel-heading">Edit BookMark</div>

                <div class="panel-body">
                    <form method="POST" action="{{ url('/') }}/{{ $bookMark->path() }}/edit">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for='title'>Title:</label>
                            <br>
                            <input style="width:400px" type="text" name="title" class="form-cotrol" value="{{$bookMark->title}}" required/>
                        </div>
                        <div class="form-group">
                            <label for='title'>Url:</label>
                            <br>
                            <input style="width:400px" type="text" name="url" class="form-cotrol" value="{{ $bookMark->url_online}}" required/>
                        </div>
                        <button type="submit" class="btn btn-success"> Save </button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection