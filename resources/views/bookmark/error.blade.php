@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row">
            <div class="panel panel-default text-center">
                <div class="panel-heading"><h4>Error</h4></div>
                    <h6>               
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </h6>
            </div>
        </div>
    </div>
@endsection