@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                
                <div class="panel-heading">Create BookMark</div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel-body">
                    <form method="POST" action="{{ url('/bookmark') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for='title'>Title:</label>
                            <br>
                            <input style="width:400px" type="text" name="title" class="form-cotrol" placeholder="Enter Name Title Url" maxlength="255" required/>
                        </div>
                        <div class="form-group">
                            <label for='title'>Url:</label>
                            <br>
                            <input style="width:400px" type="text" name="urlonline" class="form-cotrol" placeholder="Enter Url" required/>
                        </div>
                        <button type="submit" class="btn btn-success"> Save </button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection