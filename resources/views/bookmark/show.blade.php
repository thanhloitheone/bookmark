@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row">
            <div class="panel panel-default text-center">
                <div class="panel-heading"><h4>{{ $bookMark->title }}</h4></div>
                <h6> {{ $bookMark->url_online }} </h6>
            </div>

            <div class="col-md-12">
                <embed class="col-md-12" style="height:500px" src="{{ $bookMark->urlfile() }}" />
            </div>
        </div>
    </div>
@endsection