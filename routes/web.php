<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookMarkController@index');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/bookmark', 'BookMarkController@index')->name('bookmark');
Route::post('/bookmark', 'BookMarkController@store');
Route::get('/bookmark/create', 'BookMarkController@create');
Route::get('/bookmark/{bookMark}', 'BookMarkController@show');